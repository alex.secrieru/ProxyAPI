﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class ProxyController : Controller
    {
        /// <summary>
        /// Receive json -> converting to xml and send to systemA 
        /// </summary>
        /// <param name="dataAndFilter"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ServerResponse>> SendToSystemAFromSystemB(DataAndFilter dataAndFilter)
        {
            var response = new ServerResponse();
            try
            {
                //var json = JsonConvert.DeserializeObject<DataAndFilter>(dataAndFilter);
                //Convert json to xml 
                var result = await new bl().ConvertJSONtoXML(dataAndFilter);
                if (result.existError == false)
                {
                    //send xml to serverA 
                    var resultSend = await new bl().SendToSystemAFromSystemB(result.xmlDocument);
                    if (resultSend.existError == false)
                    {
                        return Ok(result.xmlDocument);
                    }
                    else
                    {
                        return StatusCode(500, resultSend.errorMessage);
                    }
                }
                else
                {
                    return StatusCode(500, result.errorMessage);
                }

                
            }
            catch (Exception ex)
            {
                response.IsError = true;
                response.ErrorMessage = ex.Message;
                return BadRequest(response);
            }
            
        }
        /// <summary>
        /// Get xml from system A then convert to json 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ServerResponse>> GetFromSystemA()
        {
            var response = new ServerResponse();
            try
            {

                //Send request to systemA and receive xml document 
                var result = await new bl().GetFromSystemAXmlDocument();
                if (result.existError == false)
                {
                    //Convert xml to json 
                    var dataAndFilter = await new bl().ConvertXMLtoJSON(result.xmlDocument);
                    if (dataAndFilter.existError == false)
                    {
                        //return json to systemB
                        response.ResponseXMLJSON = JsonConvert.SerializeObject(dataAndFilter.json);
                        response.ResponseType = ResponseType.json.ToString();
                        return Ok(response);
                    }
                    else
                    {
                        response.IsError = dataAndFilter.existError;
                        response.ErrorMessage = dataAndFilter.errorMessage;
                        return StatusCode(500, response);
                    }
                }
                else
                {
                    response.IsError = result.existError;
                    response.ErrorMessage = result.errorMessage;
                    return StatusCode(500, response);
                }

            }
            catch (Exception ex)
            {
                response.IsError = true;
                response.ErrorMessage = ex.Message;
                return  BadRequest(response);
            }
        }
    }
}