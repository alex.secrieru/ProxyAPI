﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace WebApplication1.Models
{
    public class bl
    {
        /// <summary>
        /// Send xml document with data and filter to SystemA 
        /// </summary>
        /// <param name="xDocument"></param>
        /// <returns></returns>
        public async Task<(Boolean existError, string errorMessage)> SendToSystemAFromSystemB(XDocument xDocument)
        {
            Boolean _existError = false;
            string _errorMessage ="";
            try
            {
                //HttpClient client = new HttpClient();
                //var response = await client.GetAsync("https://systema.net/api/SetXmlDocument");
                //var responseString = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                _existError = true;
                _errorMessage = ex.Message;
            }
            return (_existError, _errorMessage);
        }
        /// <summary>
        /// Get xml data from systemA 
        /// </summary>
        /// <returns></returns>
        public async Task<(XDocument xmlDocument, Boolean existError, string errorMessage)> GetFromSystemAXmlDocument()
        {
            XDocument _xmlDocument = null;
            Boolean _errorExist = false;
            string _errorMessage = "";
            try
            {
                //HttpClient client = new HttpClient();
                //var response = await client.GetAsync("https://systema.net/api/getXmlDocument");
                //var responseString = await response.Content.ReadAsStringAsync();
                //var responseString = @"<view select='true' check='false' expandMode='MasterDetail' actions='true' icon='true' preview='false'>< header title = '' />< description title = '' />< row recordtypefield = 'IncidentTypeId' imagetype = 'IncidentType' keyfield = 'id' >< cell title = 'Incident No' name = 'IncidentNr' align = 'center' width = '100' />< cell title = 'Subject' name = 'Subject' />< cell title = 'Posted On' name = 'CreatedOn' align = 'center' width = '135' />< cell title = 'Posted By' name = 'CreatedBy' width = '125' showicon = 'true' />< cell title = 'Assign To' name = 'AssignTo' width = '100' />< cell title = 'Type' name = 'IncidentTypeId' align = 'center' width = '100' />< cell title = 'Priority' name = 'PriorityCodeId' align = 'center' width = '75' />< cell title = '' name = 'HasScreenshot' imagetype = 'HasImage' width = '25' /></ row >< fetch >< sort >< column name = 'CreatedOn' ascending = 'false' />< column name = 'PriorityCodeId' ascending = 'false' /></ sort >< filter >< condition value = '8' operator= 'eq' attribute = 'IncidentStatusId' />< condition value = '' operator= 'eq-appid' attribute = 'ApplicationId' /></ filter ></ fetch >< rowstyle >< style value = 'color:#011AF3' >< condition value = '' operator= 'today' attribute = 'CreatedOn' /></ style ></ rowstyle >< masterdetail title = '' window = '' handler = '' parentKey = 'ObjectId' view = 'default' table = 'AX_Incidents_Comments' /></ view > ";
                var responseString = System.IO.File.ReadAllText("t.txt");


                //Validate xml.
                var validateXml = IsValidXml(responseString);
                //if (validateXml.existError == false)
                //{
                    _xmlDocument = XDocument.Load("t.txt");
                //}
                //else
                //{
                //    _errorExist = validateXml.existError;
                //    _errorMessage = validateXml.errorMessage;
                //}
            }
            catch (Exception ex)
            {
                _errorExist = true;
                _errorMessage = ex.Message;
            }
            return (_xmlDocument, _errorExist, _errorMessage);
        }
        /// <summary>
        /// Validate XML format 
        /// </summary>
        /// <param name="xml">Xml string</param>
        /// <returns></returns>
        public (Boolean existError,string errorMessage) IsValidXml(string xml)
        {
            Boolean _existError = false;
            string _errorMessage ="";
            try
            {
                var doc =XDocument.Load(xml);
                
                
            }
            catch (FormatException fex)
            {
                _existError = true;
                _errorMessage = fex.Message;
                //TODO : Add message NLOG  
            }
            catch (Exception ex)
            {
                _existError = true;
                _errorMessage = ex.Message;
                //TODO : Add message NLOG  
            }
            return (_existError,_errorMessage);
        }
        /// <summary>
        /// Validate JSON format 
        /// </summary>
        /// <param name="json">Json string </param>
        /// <returns></returns>
        public Boolean IsValidJson(string json)
        {
            Boolean _result = false;
            try
            {
                if ((json.StartsWith("{") && json.EndsWith("}")) || //For object
               (json.StartsWith("[") && json.EndsWith("]"))) //For array
                {
                    var _doc = JsonConvert.DeserializeObject(json);
                    _result = true;
                }
            }
            catch (FormatException fex)
            {
                //TODO : Add message NLOG  
            }
            catch (Exception ex)
            {
                //TODO : Add message NLOG  
            }
            return _result;
        }
        
        public async Task<(XDocument xmlDocument,Boolean existError,string errorMessage)> ConvertJSONtoXML(DataAndFilter dataAndFilter)
        {
            //TODO: Only for test 
            
            var xml= await GetFromSystemAXmlDocument();
            var json=await ConvertXMLtoJSON(xml.xmlDocument);
            dataAndFilter = json.json;

            XDocument xmlDoc = new XDocument();
            Boolean _existError = false;
            string _errorMessage = "";
            try
            {
                xmlDoc.Add(
                    new XElement("view", new XAttribute("select", true), new XAttribute("check", true), new XAttribute("expandMode", "MasterDetail"), new XAttribute("actions", true), new XAttribute("icon", true), new XAttribute("preview", false),
                    new XElement("header", new XAttribute("title", "")),
                    new XElement("description", new XAttribute("title", "")),
                    new XElement("row", new XAttribute("recordtypefield", "IncidentTypeId"), new XAttribute("imagetype", "IncidentType"), new XAttribute("keyfield", "id"),
                        dataAndFilter
                        .rowCells
                        .Select(x => new XElement("cell",
                                        new XAttribute("title", x.title),
                                        new XAttribute("align", x.align),
                                        new XAttribute("imagetype", x.imagetype),
                                        new XAttribute("name", x.name),
                                        new XAttribute("showicon", x.showicon),
                                        new XAttribute("width", x.width)
                                        ))),
                    new XElement("fetch", new XElement("sort",
                        dataAndFilter
                        .sorts
                        .Select(x => new XElement("column",
                                    new XAttribute("name", x.columnName),
                                    new XAttribute("ascending", x.ascending)))),
                                    new XElement("filter",
                        dataAndFilter
                        .filters
                        .Select(x => new XElement("condition",
                                    new XAttribute("value", x.condition),
                                    new XAttribute("operator", x.oper),
                                    new XAttribute("attribute", x.attribute))))),
                    new XElement("rowstyle",
                        dataAndFilter
                        .rowStyles
                        .Select(x => new XElement("style", new XAttribute("value", x.style),
                        dataAndFilter
                        .rowStylesConditions
                        //.Where(w=>w.styleNumber==x.styleNumber)
                        .Select(s => new XElement("condition",
                            new XAttribute("value", s.condition),
                            new XAttribute("attribute", s.attribute),
                            new XAttribute("oper", s.oper),
                            new XAttribute("styleNumber", s.styleNumber)
                            ))
                        ))
                    ))
                );
            }
            catch (Exception ex)
            {
                _existError = true;
                _errorMessage = ex.Message;
            }
            return (xmlDoc,_existError,_errorMessage);
        }
        public async Task<(DataAndFilter json,Boolean existError, string errorMessage)> ConvertXMLtoJSON(XDocument xmlDocument)
        {
            DataAndFilter _dataAndFilter = new DataAndFilter();
            Boolean _existError = false;
            string _errorMessage="";
            try
            {
                
                _dataAndFilter.rowCells = xmlDocument.Descendants().Elements("row").Elements("cell").Select(s => new RowCell
                {
                    name = s.Attribute("name") != null ? s.Attribute("name").Value : "",
                    title = s.Attribute("title") != null ? s.Attribute("title").Value : "",
                    align = s.Attribute("align") != null ? s.Attribute("align").Value : "",
                    imagetype = s.Attribute("imagetype") != null ? s.Attribute("imagetype").Value : "",
                    width = s.Attribute("width") != null ? s.Attribute("width").Value : "",
                    showicon = s.Attribute("showicon") != null ? s.Attribute("showicon").Value : ""
                }).ToList();
                _dataAndFilter.sorts = xmlDocument.Descendants().Elements("fetch").Elements("sort").Elements("column")
                    .Select(s => new Sort
                    {
                        columnName = s.Attribute("name") != null ? s.Attribute("name").Value : "",
                        ascending = s.Attribute("ascending") != null ? s.Attribute("ascending").Value : ""
                    }).ToList();
                _dataAndFilter.filters = xmlDocument.Descendants().Elements("fetch").Elements("filter").Elements("condition")
                    .Select(s => new Filter
                    {
                        condition = s.Attribute("value") != null ? s.Attribute("value").Value : "",
                        oper = s.Attribute("operator") != null ? s.Attribute("operator").Value : "",
                        attribute = s.Attribute("attribute") != null ? s.Attribute("attribute").Value : ""
                    }).ToList();
                _dataAndFilter.rowStylesConditions = xmlDocument.Descendants().Elements("rowstyle").Elements("style").Elements("condition")
                    .Select(s => new RowStyleCondition
                    {
                        condition = s.Attribute("value") != null ? s.Attribute("value").Value : "",
                        attribute = s.Attribute("attribute") != null ? s.Attribute("attribute").Value : "",
                        oper = s.Attribute("operator") != null ? s.Attribute("operator").Value : ""
                    }).ToList();
                _dataAndFilter.rowStyles = xmlDocument.Descendants().Elements("rowstyle").Elements("style")
                    .Select(s => new RowStyle
                    {
                        style = s.Attribute("value") != null ? s.Attribute("value").Value : ""
                    }).ToList();
                foreach (var element in xmlDocument.Descendants())
                {
                    Console.WriteLine($"{element.Name}");

                    foreach (var atribute in element.Attributes())
                    {
                        Console.WriteLine($"{atribute.Name}= {atribute.Value}");
                    }
                }
                
            }
            catch (Exception ex)
            {
                _existError = true;
                _errorMessage = ex.Message;
                
            }
            return (_dataAndFilter, _existError, _errorMessage);
        }
    }
}
