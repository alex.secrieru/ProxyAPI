﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ServerResponse
    {
        public Boolean IsError { get; set; } = false;
        public string ErrorMessage { get; set; }
        public string ResponseType { get; set; }
        public string ResponseXMLJSON { get; set; } 
    }
    public class XmlTablee
    {
        public string XmlTable { get; set; }
    }
    //public class Filter
    //{
    //    public string FieldName { get; set; }
    //    public string Operator { get; set; }
    //    public ValyeType Valye { get; set; }
    //}
    public enum ResponseType
    {
        json,
        xml
        
    }
    public class DataAndFilter
    {
        public List<RowCell> rowCells { get; set; }
        public List<Sort> sorts { get; set; }
        public List<Filter> filters { get; set; }
        public List<RowStyle> rowStyles { get; set; }
        public List<RowStyleCondition> rowStylesConditions { get; set; }
    }
    public class Sort
    {
        public string columnName { get; set; }
        public string ascending { get; set; }
    }
    public class Filter
    {
        public string condition { get; set; }
        public string oper { get; set; }
        public string attribute { get; set; }

    }
    public class RowCell
    {
        public string title { get; set; }
        public string name { get; set; }
        public string align { get; set; }
        public string width { get; set; }
        public string showicon { get; set; }
        public string imagetype { get; set; }
    }
    public class RowStyle
    {
        public int styleNumber { get; set; }
        public string style { get; set; }
       
    }
    public class RowStyleCondition
    {
        public int styleNumber { get; set; }
        public string condition { get; set; }
        public string oper { get; set; }
        public string attribute { get; set; }
    }
}
